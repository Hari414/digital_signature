<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_web extends CI_Controller {

	


	public function __construct()
    {
        parent::__construct();
        $this->load->database(); 
        $this->load->library('session');
        //$this->load->session();
        $this->load->model('post_web_model');
        $this->load->library("pagination");

    }

    function index(){
        $this->load->view('login.php');
  }

    function user_login(){
        $data = $this->input->post();

        if( !empty($data) ){
            $result = $this->post_web_model->user_login($data);
            if($result == 1 ){
                //redirect('Login/date_file_load');
                echo "Login Successfully";
            }
            if($result == 2){

            	echo "Email id and passwords are Not Matching";
           // $this->session->set_flashdata('msg', 'Fail to login.Please try again...!');
             //   redirect('');
            }
        }else{ echo "No data Available"; }
    }



	
	
	public function insert_postweb()
	{
	$method=$_SERVER['REQUEST_METHOD'];
        if($method!='POST'){

            echo json_encode(array('status' => 400,'results' => 'Bad request.'));
        }
		else
		{
			$response['status']=200;
           
			$params = $_POST;

			 $parameters=array(
			 	'UserName' => $params['UserName'],
			 	'DocumentTitle' => $params['DocumentTitle'],
			 	'NatureOfDocument' => $params['NatureOfDocument'],
			 	'Purpose' => $params['Purpose'],
			 	'Noofpages' => $params['Noofpages'],
			 	'Department' => $params['Department']
			 );

			
			$result = $this->post_web_model	->insert_postweb($parameters);
			if($result)
			{
				echo json_encode(array('status' => 200,'results' => 'Inserted'));	
			}else
			{
				echo json_encode(array('status' => 201,'results' => 'Not Inserted.'));
			}
		}			
	}


      public function select()  
      {  
         
         
         //return the data in view  
          

         $config = array();
        $config["base_url"] = "http://localhost/digital_signature/Post_web/select";
        $config["total_rows"] = $this->post_web_model->get_count();
        $config["per_page"] = 2;
        $config["uri_segment"] = 1;

       $this->pagination->initialize($config);

       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    

        $data["links"] = $this->pagination->create_links();

        //$data['authors'] = $this->post_web_model->get_authors();
 		$data['h']=$this->post_web_model->select($config["per_page"], $page); 
        $this->load->view('Post_web_view', $data); 


      }  


	
	
}
